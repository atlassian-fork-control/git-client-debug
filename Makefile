PREFIX ?= /usr/local
MANPREFIX ?= "$(PREFIX)/share/man/man1"
SCRIPTS = $(wildcard bin/git-*)
MAN_PAGES = $(wildcard man/git-*.1)
DOCS_SOURCE = $(wildcard docs/git-*.md)
MAN_TARGET := $(patsubst %.md,%.1,$(DOCS_SOURCE))

.PHONY: install uninstall

install:
	@mkdir -p $(DESTDIR)$(MANPREFIX)
	@mkdir -p $(DESTDIR)$(PREFIX)/bin
	@$(foreach BIN, $(SCRIPTS), \
		echo "Installing `basename $(BIN)` -> $(DESTDIR)$(PREFIX)/bin"; \
		cp -f $(BIN) $(DESTDIR)$(PREFIX)/$(BIN); \
	)
	@$(foreach MAN, $(MAN_PAGES), \
		echo "Installing `basename $(MAN)` -> $(DESTDIR)$(MANPREFIX)"; \
		cp -f $(MAN) $(DESTDIR)$(MANPREFIX)/`basename $(MAN)`; \
	)

uninstall:
	@$(foreach BIN, $(SCRIPTS), \
		echo "Removing $(DESTDIR)$(PREFIX)/$(BIN)"; \
		rm -f $(DESTDIR)$(PREFIX)/$(BIN); \
	)
	@$(foreach MAN, $(MAN_PAGES), \
		echo "Removing $(DESTDIR)$(MANPREFIX)/$(MAN)"; \
		rm -f $(DESTDIR)$(MANPREFIX)/`basename $(MAN)`; \
	)

docs : $(MAN_TARGET)

%.1 : %.md
		pandoc -s -w man $< -o man/`basename $@`
	
