# Debug wrapper for the git clone and push commands

The `./bin` directory contains a set of wrapper scripts that capture valuable
debug information when attempting to clone from or push to a git repository.

# Installation

The scripts in the bin directory can be used directly (e.g.
`./bin/git-debug-clone URL`) or as a git subcommand if the scripts are in
a directory that is available on the $PATH.

    curl -OL https://bitbucket.org/atlassian/git-client-debug/raw/master/bin/git-debug-clone
    ./git-debug-clone URL

Alternatively `make` can be used to copy the scripts to your /usr/local/bin
directory. This will also copy the man pages to the appropriate directories on
your system.

    make install

To remove the scripts and man pages run uninstall:

    make uninstall

# Usage

Use `git debug-clone` instead of `git clone` and `git debug-push` instead of
`git push` to capture debug information that will be written to a local log
file.

Clone:

    git debug-clone ssh://git@bitbucket.org/atlassian/git-client-debug.git

Push:

    git debug-push origin master

# Log file

The wrapper scripts capture stderr and stdout and write both to the file
`atlassian-stash-git-debug.log` in the current working directory.
